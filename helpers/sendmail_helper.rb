require 'date'

helpers do
  def sending_mail(ipaddress)
    Pony.mail(:to => "gerardogc2378@gmail.com",
      :from => "gerardogc2378@gmail.com",
      :subject => "ggcspace", 
      :body => "New Visitor at #{ipaddress} ... #{DateTime.now.strftime("%m/%d/%Y %R")}",
      :via => :smtp,
        :via_options => { 
          :address => "smtp.gmail.com",
          :port => "587",
          :user_name => "your-email", 
          :password => "your-password", 
          :authentication => :plain, 
          :domain => "NA"
        }
    )
    return true
  rescue Exception => e
    return false
  end
end
